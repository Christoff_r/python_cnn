# Convolutional Neural Network (CNN) to classify handwritten digits

![banner](https://imgs.search.brave.com/60tG1vpz7M3i9zDrc3CzAux2eHPlkwaYJ6qd9z7jyWw/rs:fit:888:225:1/g:ce/aHR0cHM6Ly90c2Uy/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5t/MEJOZ3QwNHlPYklD/VlR5WWJQUUtnSGFE/OSZwaWQ9QXBp)

![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)
![ml](https://img.shields.io/badge/ml-cnn-lightgrey)
![language](https://img.shields.io/badge/language-python-yellow)
![notebook](https://img.shields.io/badge/jupyter-notebook-orange)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Accuracy using different models, batch sizes and epochses](#accuracy-using-different-models-batch-sizes-and-epochses)
- [Contributing](#contributing)
- [License](#license)


## Background

This simple cnn is an introduction to using ml to classify handwritten digtis, in the range from 0-9.

The algorithm takes in images with a size 28x28 pixels in grayscale format.

## Install
Below is a short description on how to install the program.

### Dependencies
The program uses some python librarys in order to work. All librarys can be installed using pip or anaconda. 
The librarys are:

- Jupyter Notebook
- NumPy
- Pandas
- TensorFlow
- Keras (comes with tensorflow)
- Matplotlib

### Cloneing from gitlab

Navigate to were you want the repository to be. Open the `CLI` a type the following.

```
git clone https://gitlab.com/Christoff_r/python_cnn.git
```

## Usage

Open the `cnn.ipynb` in your editor of choise and run all the cells. 

## Accuracy using different models, batch sizes and epochses.

### Model 1 - batch size 128 - epochses 5
![model_1](../graph/model_1.png)

### Model 2 - batch size 1 - epochses 5
![model_2](../graph/model_2.png)

### Model 3 - batch size 33600  - epochses 5
![model_3](../graph/model_3.png)

### Model 4 - batch size 128 - epochses 5 - normalized pixel values
![model_4](../graph/model_4.png)

### Model 5  - batch size 128 - epochses 10 - normalized pixel values
![model_5](../graph/model_5.png)

### Model 6 - batch size 128 - epochses 20 - normalized pixel values
![model_6](../graph/model_6.png)

### Model 7 - batch size 33600 - epochses 69 - normalized pixel values
![model_7](../graph/model_7.png)

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`
